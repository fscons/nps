from django.db import models

# Create your models here.
class Answer(models.Model):
     uuid = models.CharField(max_length=36)
     score = models.IntegerField(blank=True, null=True)
     comment = models.TextField()
