from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Answer
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404


def answer(request, uuid, score):
    score = int(score)
    if score > 10:
        raise Http404
    a,_ = Answer.objects.get_or_create(uuid=uuid)
    a.score = int(score)
    a.save()
    template = loader.get_template('nps/comment.html')
    context = RequestContext(request, {
        'uuid': a.uuid,
        'score': a.score,
    })
    return HttpResponse(template.render(context))


def comment(request, uuid):
    a = get_object_or_404(Answer, uuid=uuid)
    comment = request.POST['comment']
    a.comment = comment
    a.save()
    template = loader.get_template('nps/thanks.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

