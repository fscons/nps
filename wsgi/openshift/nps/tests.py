from django.test import TestCase
from django.test import Client
from .models import Answer

class AnswerTestCase(TestCase):
    # def setUp(self):
    #     Animal.objects.create(name="lion", sound="roar")
    #     Animal.objects.create(name="cat", sound="meow")

    def test_valid_answer(self):
        """A valid answer is correctly saved"""
        c = Client()
        response = c.get('/abc/1')
	a = Answer.objects.get(uuid='abc')
	self.assertEqual(a.score, 1)

    def test_wrong_score(self):
        c = Client()
        response = c.get('/abc/11')
        self.assertEqual(response.status_code, 404)

class CommentTestCase(TestCase):
    def setUp(self):
        Answer.objects.create(uuid='abc', score=9)

    def test_coment(self):
        c = Client()
        comment = 'Keep calm and carry on'
        response = c.post("/abc/comment", {'comment': comment})
	a = Answer.objects.get(uuid='abc')
        self.assertEqual(a.comment, comment)

