from django.conf.urls import patterns, include, url

from django.contrib import admin

import nps.views


admin.autodiscover()

	

urlpatterns = patterns('',
    url(r'^$', 'views.home', name='home'),
    url(r'^(?P<uuid>\w+)/(?P<score>\d+)$', nps.views.answer, name='answer'),
    url(r'^(?P<uuid>\w+)/comment$', nps.views.comment, name='comment'),
    url(r'^admin/', include(admin.site.urls)),
)
